## Завдання

Зверстати сторінку з анімацією, як на малюнку [gif](animation_front-end_wars.gif). 

#### Технічні вимоги до верстки

- Анімація має тривати 30 секунд, потім починатися спочатку

#### Текст (можна використати свій варіант):

##### EPISODE 0
##### ATTACK OF THE FRONTEND

When there is no more hope. When Winter is coming. When the White Walkers prepare to break down the Wall and JAVA can no longer stop them ... <br><br>
Comes he - Javascript. Lord of untyped data and components, he does not give up technical tasks "to do nicely" and is not afraid of calculations on the user side. But he comes not alone, having 3 of his faithful friends with him: jQuery, React.js and Node.js.

#### Необов'язкове завдання

- Додати на сторінку аудіо тег з треком-заставкою з [Star Wars](https://www.youtube.com/watch?v=EjMNNpIksaI) :)

#### Примітка
- Верстка повинна бути виконана без використання бібліотек CSS типу Bootstrap або Materialize.
– Не намагайтеся зробити autoplay. Подібні спроби будуть припинені більшістю браузерів.

